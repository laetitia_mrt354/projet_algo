import time
import sys


# This class allows us to monitor the time spent by a function
class Timer():
    # Function called right before the execution of the function
    def __enter__(self):
        self.t1 = time.perf_counter()
        print('Program starting')
        return self

    # Function called right after the function
    def __exit__(self, type, value, traceback):
        print('Program ending')
        self.t2 = time.perf_counter()
        #self.t = self.t2 - self.t1
        #print('Program finished')

    # Function that prints on the shell the time spent by the instructions
    def print_time(self, template: str = "{}"):
        print(template.format(round(self.t2 - self.t1, 2)))


def update_progress(progress):
    barLength = 50  # Modify this to change the length of the progress bar
    status = ""
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\rPercent: [{0}] {1}% {2}".format( "#"*block + "-"*(barLength-block), round(progress*100,2), status)
    sys.stderr.write(text)
    sys.stderr.flush()

if __name__ == "__main__":
    # Exemple how to use this class
    with Timer() as total_time:  # time all instructions in the ’with’ statements
        for i in range(5):
            time.sleep(0.1)
    total_time.print_time("Durée = {} secondes")