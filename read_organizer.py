""" Sorting method for reads"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from argparse import ArgumentParser
import os
import matplotlib.pyplot as plt
import psutil
from timer import Timer

                       ####################
                       ##### METHOD 1 #####
                       ####################

def multi_seed_sorting(input_file:str, output_file:str, n_seeds:int = 1,
                       seed_size:int = 10, ram:bool = False):
    """ Reads a input file containing multiple reads (Nt sequences) and
    returns a file containing all input reads ordered by proximity.
    Proximity is calculated using extracted seeds from each read that are
    then sorted using the seed_sorting() function.

    Args:
        input (str): path to an input file.
        output (str): path to an output file.
        n_seeds (int, optional): number of seeds to extract from each read. Defaults to 1.
        seed_size (int, optional): number of caracters in the seed. Defaults to 7.
        ram (bool, optional): True to return current ram used during execution. Defaults to False

    Return:
        cpu_usage (int)
    """

    with open(input_file, 'r', encoding="utf-8") as file:
        seeds_reads: dict = {}
        for read in file:
            if read[0] == ">": # ignores comments
                continue
            try:
                seeds_reads[tuple(seed_extracting(read.strip(),
                                  n_seeds, seed_size))] += [read.strip()]
            except:
                seeds_reads[tuple(seed_extracting(read.strip(),
                                  n_seeds, seed_size))] = [read.strip()]

    cpu_usage = psutil.Process(os.getpid())
    cpu_usage = cpu_usage.memory_info().rss

    with open(output_file, 'w', encoding="utf-8") as file:
        for seeds in seed_sorting(seeds_reads, n_seeds):
            if len(seeds_reads[seeds]) > 1:
                for read in seeds_reads[seeds]:
                    file.write(read + "\n")
            else:
                file.write(str(seeds_reads[seeds][0]) + "\n")

    if ram:
        return cpu_usage

def seed_extracting(read: str, n_seeds: int, seed_size: int = 7):
    """ Extracts a given number of seeds from a read, i.e. fragment(s)
    extracted from the read according, here, to the minimizer() function.

    Args:
        read (str): input single read (list of nucleotides).
        n_seeds (int): number of seeds to extract in the sequence.
        seed_size (int, optional): lenght of the seeds (in number of nucleotides). Defaults to 7.

    """

    return sorted([minimizer(read[len(read)//n_seeds * i:len(read)//n_seeds * (i+1)], seed_size)
                   for i in range(n_seeds)])


def minimizer(chunk:str, seed_size:int):
    """ Finds the minimizer (smallest seed in lexicographic order)
    inside a chunk of the read.
    Args:
        chunk (str): sequence chunk
        seed_size (int): number of nucleotides by seed
    Return:
        minimal seed (str): minimizer of this chunk
    """

    minimal_seed = chunk[0: seed_size]
    for i in range(1, len(chunk[0:-seed_size+1])):
        new_seed = chunk[i: i+seed_size]
        if sorted([new_seed, minimal_seed])[0] == new_seed:
            minimal_seed = new_seed

    return minimal_seed


def seed_sorting(keys:list, n_seeds):
    """ Sorts the extracted seeds. First sorts according to the
    first seed, then inside each 'cluster' sorts again according
    to the second seed and inside each 'subcluster' according to
    the third seed.

    Args:
        seeds_reads (dict): dict with tuple as key corresponding to
                            the seed of one read, and the corresponding read as value
        n_seeds (_type_): number of seeds in each read

    Returns:
        Order list of tuples corresponding to the seeds.
        Each tuple correspond to the seeds of one read.
    """

    return sorted(keys, key=lambda x:[x[i] for i in range(n_seeds)])


                       ####################
                       ##### METHOD 2 #####
                       ####################

def sorting_by_pairs(input_file:str, output_file: str,
                     pairs_ofInterest: list=['GA', 'TC', 'GC', 'GG'],
                     plotting: bool=False, ram: bool=False):
    """Main read organizer function which takes a set of reads,
       sorts it using occurrences of adjacent pairs of nucleotides
    and rewrites it sorted.
    Args:
        input (str): name of input file containing a set of reads to be sorted.
        output (str): name of output file to be written.
        pairs_ofInterest (list): list of pairs of nucleotides to study.
                                 Defauts to ['AA','AT', 'TA', 'TT'].
        plotting (bool, optional): specifies to plot or not the
                                   pairs distribution of reads.
                                   Defaults to False.
    """
    pair_index = {'AA': 0, 'AC': 1, 'AG': 2, 'AT': 3,
                  'CA': 4, 'CC': 5, 'CG': 6, 'CT': 7,
                  'GA': 8, 'GC': 9, 'GG': 10, 'GT': 11,
                  'TA': 12, 'TC': 13, 'TG': 14, 'TT': 15
                 }

    with open(input_file, 'r', encoding='utf_8') as file:
        pairs_reads = {tuple(pairs_computing(read.strip(), pair_index)) : read.strip()
                       for read in file if read[0] != ">"}
        psutil.cpu_percent(4)

    if plotting:
        plot_pair_distribution(pairs_reads)

    cpu_usage = psutil.Process(os.getpid())
    cpu_usage = cpu_usage.memory_info().rss

    with open(output_file, 'w', encoding="utf-8") as file:
        for pair in pairs_sorting(pairs_reads, pairs_ofInterest, pair_index):
            file.write(pairs_reads[pair] + "\n")

    if ram:
        return cpu_usage

def pairs_sorting(keys: list, pairs_ofInterest: list, pair_index: dict):
    """Algorithm that sorts a given set of read by comparing
       the number of occurences of pairs of nucleotides.
    Sort using first pair in pairs_ofInterest, then second one in this 'cluster', etc.


    Args:strip
        pairs_occ (list): tuple list of pairs occurences for all reads
        caracteristic_pairs (list): list of pairs of interest,
                                    determined with the plot of the pairs ditribution
        pair_index(dict): dictionnary of pairs associated to
                          its position in the list of occurences

    Returns:
        (list): ordered tuple list des occ de paires triées #nope
    """

    return sorted(keys, key=lambda x: [x[pair_index[pair.strip()]] for pair in pairs_ofInterest])

def pairs_computing(read: str, pair_index: dict):
    """Creates the list of pairs occurences of Nt for a given read

    Args:
        read (str):
        pair_index (dict): dictionnary of pairs associated to
                           its position in the list of occurences

    Returns:
        pairs_occ (list): list of pairs occurences
    """
    pairs_occ: list = [0] * 16
    for i in range(len(read)-1):
        try: # in case of "N", "-" or else
            pairs_occ[pair_index[f"{read[i]}{read[i+1]}"]] += 1
        except:
            continue
    return pairs_occ


def plot_pair_distribution(pairs_reads:dict):
    """Generates histograms of the distribution of
       the pairs occurences of every reads.

    Args:
        pairs_reads (dict): Dictionnary containing, list of pairs occurences in key
                            and the associated read in value
    """
    pairs_names = [x + y for x in ['A', 'C', 'G', 'T'] for y in ['A', 'C', 'G', 'T']]
    plt.figure(figsize=(18, 16), dpi= 200, facecolor='w', edgecolor='k')
    plt.suptitle('Number of pair occurence by reads')

    for i in range(16):
        plt.subplot(4, 4, i+1)
        plt.hist([pair[i] for pair in list(pairs_reads.keys())])
        plt.xlabel(pairs_names[i])
    plt.savefig('results/distrib_occ.png')


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("input", help="specifies the input file", type=str)
    parser.add_argument("output", help="specifies the output file", type=str)

    args = parser.parse_args()
    with Timer() as time:
        multi_seed_sorting(args.input, args.output)
    time.print_time("Durée = {} secondes")
