""" Main execution file """
from os import listdir, path, system
from pathlib import Path
from argparse import ArgumentParser
from matplotlib import pyplot as plt
import numpy as np
from read_organizer import multi_seed_sorting, sorting_by_pairs
from timer import Timer


def get_compression() -> int:
    """Get last row of param.log, corresponding to
       size of the computed compressed file

    Returns:
        compression (int): size of the file
    """
    with open("results/param.log", 'r', encoding="utf-8") as logfile:
        for row in logfile:
            pass
        compression:int = int(row.split('\t')[0])
        return compression

def plot_by_method(compressions_m1:list, compressions_m2:list, title:str, output:str) -> None:
    """Plot results (execution time / compression ratio) by methods

    Args:
        compressions_m1 (list): Compression ratio values for method 1
        compressions_m2 (list): Compression ratio values for method 2
        title (str): figure title
        output (str): output path for the figure
    """
    plt.figure(figsize=(8, 8))
    plt.title(title)
    xlabels = ['5', '10', '20', '40', '80', '120']
    if len(compressions_m1) > 0:
        plt.plot(xlabels, sorted(compressions_m1), label='method 1')
    if len(compressions_m2) > 0:
        plt.plot(xlabels, sorted(compressions_m2), label='method 2')
    plt.legend()
    plt.savefig(output)


def plot_params(compressions: list, output:str='results/plot_params.png') -> None:
    """Plotting compression ratio function of method 1 parameters

    Args:
        compressions (list): compression ratio for all parameters
        output (str, optional): output path to the figure. Defaults to 'results/plot_params.png'.
    """
    colors = ['mediumspringgreen','turquoise','teal', 'cornflowerblue',
              'slateblue', 'darkviolet', 'purple']
    nb_seed = np.arange(1, 6)
    dico_params =  {i+1:compressions[i*5:(i + 1)*5] for i in range(7)}
    pos = 1
    for key in dico_params:
        plt.bar(nb_seed + 0.05*pos - 0.25,
                dico_params[key],
                width=0.1,
                label= "k = " + str(key + 3),
                color=colors[key-1])
        pos += 2

    plt.xticks(nb_seed, [f"{i+1} seeds" for i in range(5)])
    plt.title("Compression ratio function of method 1 parameters")
    plt.xlabel("Number of seeds")
    plt.ylabel("Compression ratio")
    plt.legend(loc="lower right", title="kmer length")
    plt.axis([0.5, 7, 1, 3.7]) # cadre du plot
    plt.savefig(output)


def setup_logs() -> None:
    """ Initializes the logfile """
    Path('results').mkdir(exist_ok=True)
    with open("results/param.log", 'w', encoding="utf-8") as logfile:
        logfile.write("> Compression results (in ko):\n")

def zip_original(input_name:str, output_name:str) -> None:
    """Compress original file

    Args:
        input (str): path to input file
        output (str): path to output file
    """
    system(f"grep -v \"^>\"  {input_name} | gzip > {output_name}")
    system(f"echo {output_name}: >> results/param.log")
    system(f"ls -l {output_name}" + "| awk '{print $ 5}' >> results/param.log")

def zip_sorted(output_name:str) -> None:
    """Compress sorted file

    Args:
        output (str): path to output file
    """
    system(f"gzip {output_name}")
    system(f"echo {output_name}.gz: >> results/param.log")
    system(f"ls -l {output_name}.gz" + "| awk '{print $ 5}' >> results/param.log")


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-i', '--input',type=str,
        help="Specifies the input file",
    )
    parser.add_argument('-o', '--output', type=str,
        help='Specifies output file name'
    )
    parser.add_argument('-a', '--all-data', dest='all_data', type=str,
        help='Computes and compares compression ration and execution time of all the data \
              (requires a directory as --output)'
    )
    parser.add_argument('-m1', '--method1', action="store_true",
        help='Executes method 1 (Sorting by multiple seeds)',
    )
    parser.add_argument('-m2', '--method2', action='store_true',
        help='Executes method 2 (Sorting by pairs of nucleotides)',
    )
    parser.add_argument('-c', '--compression', action="store_true",
    help='Runs compressions commands'
    )
    parser.add_argument('--distrib', action='store_true',
        help="Plots the distribution of every pairs in the set of reads",
    )
    parser.add_argument('--test_params', action='store_true',
        help="Plots the compression quality with multiple number and size of seeds",
    )
    parser.add_argument('--pairs', type=str,
        help='List of pairs used to sort the set of reads (only for method 2)',
        )
    parser.add_argument('-t', '--time', action='store_true',
        help="Plots the evolution of time and memory occupied function of file size",
    )
    parser.add_argument('-p1', '--params1', type=str, default="1, 10",
        help="Specify parameters for the method 1",
    )
    parser.add_argument('-p2', '--params2', type=str, default="AA, AT, TA, TT",
        help="Specify parameters for the method 2",
    )

    args = parser.parse_args()

    if args.clean:
        system("rm -r results/")
        raise Exception("results/ file is clean")

    if args.output is None:
        raise ValueError("Please precise an input AND an output file (or dir if -a) !")

    if args.all_data is None:
        if args.input is None:
            raise ValueError("Please precise an input AND an output file !")

    if not args.method1 and not args.method2:
        raise Exception('Please precise a method to execute (-m1, -m2). \
                         Type -help for more informations')

    setup_logs()

    ###############################################################
    ### Compression ratio and execution time for all files ###
    ###############################################################

    # Compute sorting functions on all data sets
    if args.all_data:
        Path(args.output).mkdir(exist_ok=True)
        compressions_m1: list = [] # compressions ratio for method 1
        compressions_m2: list = [] # compressions ratio for method 2
        execution_time_m1: list = [] # execution time for method 1
        execution_time_m2: list = [] #  execution time for method 2
        memory_m1: list = [] # memory allocated for method 1 execution
        memory_m2: list = [] # memory allocated for method 2 execution

        for file in listdir(args.all_data):
            filename = path.splitext(file)[0]
            input_path = f"{args.all_data}/{file}"
            output_path = f"{args.output}/{filename}_init.gz"
            zip_original(input_path, output_path) # zipping initial file
            original_compression = get_compression()

            if args.method1:
                n_seeds = int(args.params1.split(',')[0])
                seed_size = int(args.params1.split(',')[1])
                with Timer() as time:
                    output_path = f"{args.output}/{filename}_m1.txt"
                    memory_m1.append(multi_seed_sorting(input_path, output_path,
                                                        n_seeds, seed_size, ram=True))
                time.print_time("Durée = {} secondes\n")
                execution_time_m1.append(round(time.t2 - time.t1, 2))
                zip_sorted(output_path)
                compressions_m1.append(round(original_compression/get_compression(),3))

            if args.method2:
                pairs_ofInterest: list = args.params2.split(',')
                with Timer() as time:
                    output_path = f"{args.output}/{filename}_m2.txt"
                    memory_m2.append(sorting_by_pairs(input_path, output_path,
                                                      pairs_ofInterest, ram=True))
                time.print_time("Durée = {} secondes\n")
                execution_time_m2.append(round(time.t2 - time.t1, 2))
                zip_sorted(output_path)
                compressions_m2.append(round(original_compression/get_compression(), 3))

            if not args.method1 and not args.method2:
                raise Exception('Please precise a method to execute (-m1, -m2). \
                                 Type -help for more informations')

        # Plotting results
        plot_by_method(compressions_m1, compressions_m2,
            title = 'Plot compression ratio vs file size',
            output = 'results/compression_by_method.png')

        plot_by_method(execution_time_m1, execution_time_m2,
            title = 'Plot execution time (seconds) vs file size',
            output = 'results/cputime_by_method.png')

        plot_by_method(memory_m1, memory_m2,
            title = 'Plot memory (bytes) vs file size',
            output = 'results/memory_by_method.png')

    else:
        ### Method 1 ###
        if args.method1:
            n_seeds, seed_size = int(args.params1.split(',')[0]), int(args.params1.split(',')[1])
            output_path = f"{path.splitext(args.output)[0]}_init.gz"

            ### Compression with defined arguments ###
            if args.compression:
                with Timer() as timer:
                    multi_seed_sorting(args.input, args.output, n_seeds, seed_size)
                timer.print_time("Durée = {} secondes\n")
                zip_original(args.input, output_path)
                zip_sorted(args.output)

            ### Test compression ratio for every method 1parameters ###
            elif args.test_params:
                compressions = []
                zip_original(args.input, output_path)
                system(f"rm -r {output_path}")
                original_compression = get_compression()

                for seed_size in range(4, 11):
                    for n_seed in range(1, 6):
                        output_path = f"{path.dirname(args.output)}/{n_seed}seeds_{seed_size}"
                        multi_seed_sorting(args.input, output_path, n_seed, seed_size)
                        zip_sorted(output_path)
                        system(f"rm -r {output_path}.gz")
                        compressions.append(round(original_compression/get_compression(), 3))
                plot_params(compressions)

            ### Without compression ###
            else:
                with Timer() as timer:
                    multi_seed_sorting(args.input, args.output)
                timer.print_time("Durée = {} secondes\n")


        ### Method 2 ###
        if args.method2:
            pairs_ofInterest: list = args.params2.split(',')
            with Timer() as timer:
                if args.distrib: # Show nucleotides pairs distribution
                    sorting_by_pairs(args.input, args.output, pairs_ofInterest, plotting=True)
                else:
                    sorting_by_pairs(args.input, args.output, pairs_ofInterest)
            timer.print_time("Durée = {} secondes\n")


            if args.compression:
                output_path = f"results/{path.splitext(path.basename(args.output))[0]}_nocoms.gz"
                zip_original(args.input, output_path)
                zip_sorted(args.output)
