import unittest
from os import system

from read_organizer import *

class Test_read_organizer(unittest.TestCase):
    """ Class for unitary tests """

    ##########################################
    #### Unit tests for the first method ####
    ##########################################

    def test_minimizer(self):
        "Checks if the seed is at good length and really the smallest in the read"
        chunk = 'GCGATCGCATTGAGCCTTGCGTACTA'
        seed = minimizer(chunk, seed_size = 3)
        self.assertEqual(len(seed), 3, 'Incorrect minimizer size')
        self.assertEqual(seed, 'ACT', 'Wrong minimizer')

    def test_seed_extracting(self):
        "Checks if the correct seeds are exctracting from a read"
        read = 'ACTGATCGTACGCCTACAAGTACGTCGTCGATCGATGAATCGTATCGATGGGCTTATTATCTATCTAGTCGATGA'
        seeds = seed_extracting(read, n_seeds = 4, seed_size = 3)
        self.assertEqual(len(seeds), 4, 'Wrong number of extracted seeds')
        self.assertEqual(seeds, ['AAT','ACA', 'ACG', 'AGT'], 'Wrong seeds')

    def test_seed_sorting(self):
        "Checks if the sorting function works as it should"
        test_dict = [('ACT', 'AGC', 'ATC'),
                     ('AGA', 'AGT', 'ATT'),
                     ('ACC', 'AGC', 'ATG')]
        seeds_sorted = seed_sorting(test_dict, n_seeds = 3)
        self.assertEqual(len(seeds_sorted), len(test_dict), 'Incorrect number of tuples after sorting')
        self.assertEqual(seeds_sorted, [('ACC','AGC','ATG'),('ACT','AGC','ATC'),('AGA','AGT','ATT')], 'Incorrect sorting of seeds')

    def test_multi_seed_sorting(self):
        "Checks if the main function of method 1 is giving the right"
        sorted_file = 'test_data/test_seed_sorted.txt'
        test_file = 'test_data/true_sorting_m1.txt'
        multi_seed_sorting('test_data/reads_test.fasta', sorted_file, n_seeds = 3, seed_size=3)
        with open(sorted_file, 'r') as file:
            reads_sorted = file.readlines()
        with open(test_file, 'r') as file:
            test_sorted = file.readlines()
        self.assertEqual(reads_sorted, test_sorted, 'Wrong sorting')

    ##########################################
    #### Unit tests for the second method ####
    ##########################################

    def test_pairs_computing(self):
        "Cheks if pairs occurences are well computed"
        pair_index = {'AA': 0, 'AC': 1, 'AG': 2, 'AT': 3,
            'CA': 4, 'CC': 5, 'CG': 6, 'CT': 7,
            'GA': 8, 'GC': 9, 'GG': 10, 'GT': 11,
            'TA': 12, 'TC': 13, 'TG': 14, 'TT': 15
             }
        read: str = 'ACTGATCGTACGCCTACAAGTACGTCGTCGATCGATGAATCGTATCGATGGGCTTATTATCTATCTAGTCGATGA'
        pairs: list = pairs_computing(read, pair_index)
        self.assertEqual(len(pairs), 16, 'tuple as key should has a length of 16')
        self.assertEqual(pairs[0], 2, 'Wrong number of AA occurences' )
        self.assertNotEqual(pairs[4],  3, 'Wrong number of CA occurences')


    def test_pairs_sorting(self):
        "Checks if sorting function is sorting as wanted"
        base_set = [
            (2, 3, 4, 5, 3, 1, 5, 6, 8, 3, 5, 3, 5, 1, 3, 1),
            (1, 3, 4, 5, 6, 1, 5, 6, 9, 3, 5, 3, 5, 1, 3, 10),
            (1, 3, 4, 5, 6, 1, 5, 6, 9, 3, 5, 3, 5, 1, 3, 6),
            (2, 3, 4, 5, 4, 1, 5, 6, 1, 3, 5, 3, 5, 1, 3, 8)
            ]
        # Set sorted by first occ, 4th and last
        # corresponding to 'AA', 'AT' and 'TT'
        wanted_set = [
            (1, 3, 4, 5, 6, 1, 5, 6, 9, 3, 5, 3, 5, 1, 3, 6),
            (1, 3, 4, 5, 6, 1, 5, 6, 9, 3, 5, 3, 5, 1, 3, 10),
            (2, 3, 4, 5, 3, 1, 5, 6, 8, 3, 5, 3, 5, 1, 3, 1),
            (2, 3, 4, 5, 4, 1, 5, 6, 1, 3, 5, 3, 5, 1, 3, 8)
            ]

        pair_index = {'AA': 0, 'AC': 1, 'AG': 2, 'AT': 3,
            'CA': 4, 'CC': 5, 'CG': 6, 'CT': 7,
            'GA': 8, 'GC': 9, 'GG': 10, 'GT': 11,
            'TA': 12, 'TC': 13, 'TG': 14, 'TT': 15
             }

        self.assertEqual(pairs_sorting(base_set, ['AA', 'AT', 'TT'], pair_index), wanted_set, 'Wrong sorting')


    def test_sorting_by_pairs(self):
        "Checks if the main function for the method 2 is givind the right order"
        sorted_file = 'test_data/test_seed_sorted.txt'
        test_dir = 'test_data/true_sorting_m2.txt'
        sorting_by_pairs('test_data/reads_test.fasta', sorted_file, ['AA', 'AT', 'TT'])
        with open(sorted_file, 'r') as file:
            reads_sorted = file.readlines()
        with open(test_dir, 'r') as file:
            test_sorted = file.readlines()
        self.assertEqual(reads_sorted, test_sorted, 'Wrong sorting')


if __name__ == '__main__':
    system("python3 -m unittest -v unittest_read_organizer.py")
