# Read Organizer

Ce projet vise à créer des outils permettant de réagencer les fichiers de reads issu du séquençage, dans le but d'améliorer leur compression par un compresseur générique de type gzip. Nous avons développé 2 méthodes différentes, l'une basée sur l'extraction de seeds dans les reads et l'autre basée sur le comptage du nombre d'occurence de paires de nucléotides dans chaque read.

### Pré-requis

- Python3

### Installation

L'archive read_organisez.zip contient les deux fichiers main.py et read_organizer.py, ainsi qu'un fichier timer.py contenant une classe permettant de mesurer le temps d'exécution.
Des données test sont également disponibles dans le dossier test_data.

## Pour commencer

Le fichier read_organizer.py contient toutes les fonctions. Le fichier main.py est le fichier principal permettant l'exécution des fonctions et notamment l'utilisation de nombreux arguments dans le terminal. Les options sont les suivantes :

* -h, --help        Ouvrir l'aide
* -i INPUT          Spécifier le fichier d'entrée
* -o OUTPUT         Spécifier le fichier de sortie
* -a ALL_DATA       Exécuter les fonctions pour tout un set de données
* -m1, METHOD1      Exécuter la méthode 1 (Sorting by multiple seeds)
* -m2, METHOD2      Exécuter la méthode 2 (Sorting by pairs of nucleotides)
* -c, COMPRESSION   Utiliser les commandes de compression
* --distrib         Afficher la distribution des occurences de paires de nucléotides pour tout les reads
* --test_params     Afficher le ratio de compression obtenu avec la méthode 1 pour différentes tailles t différents nombre de seeds
* -t, --time        Afficher l'évolution du temps et de la mémoire occupée en fonction de la taille du fichier
* -p1, --params1    Tuple spécifiant le nombre et la taille des seeds pour la méthode 1. "1, 10" par défaut. Attention à bien respecter le format string.
* -p2, --params2    Liste spécifiant les paires d'intérêt à utiliser pour la méthode 2. "AA, AT, TA, TT" par défaut. Attention à bien respecter le format string.

## Tests

Des test unitaires ont été réalisés pour toutes les fonctions. Ceux-ci sont disponibles dans le fichier unittest_read_organisez.py

## Programmé avec

[VSC](https://code.visualstudio.com/) - Editeur de code

## Versions

**Dernière version :** 08.12.22

## Auteurs

* **Laetitia Martinetti** _alias_ [@laetitia_mrt354](https://gitlab.com/laetitia_mrt354)
* **Maxence Brault** _alias_ [@Max_hence](https://gitlab.com/Max_hence)

## License

Aucune
